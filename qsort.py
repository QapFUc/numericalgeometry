import matplotlib.pyplot as plt
import random
def create_segment(p1, p2, A):
    a = []
    b = []
    
    if (p2[0] - p1[0]) == 0:
        return a, b

    m = (p2[1] - p1[1]) / (p2[0] - p1[0])
    c = -m*p1[0] + p1[1]

    for i in A:
        if i[1] > m * i[0] + c:
            a.append(i)
        elif i[1] < m * i[0] + c:
            b.append(i)

    return a, b

def find_dist(p1, p2, p3):
    return abs((p1[1] - p2[1])*p3[0] + (p2[0] - p1[0])*p3[1] + p1[0]*p2[1] - p2[0]*p1[1]) / ((p1[1] - p2[1])**2 + (p2[0] - p1[0])**2) ** (1/2)

def qhull(p1, p2, segment, flag):
    res = []

    if segment == [] or p1 is None or p2 is None:
        return []

    far_dist = -1
    far_vertex = None

    for p in segment:
        dist = find_dist(p1, p2, p)
        if dist > far_dist:
            far_dist = dist
            far_vertex = p
    
    res += [far_vertex]

    segment.remove(far_vertex)

    p1a, p1b = create_segment(p1, far_vertex, segment)
    p2a, p2b = create_segment(p2, far_vertex, segment)

    if flag == 1:
        res += qhull(p1, far_vertex, p1a, 1)
        res += qhull(far_vertex, p2, p2a, 1)
    else:
        res += qhull(p1, far_vertex, p1b, -1) 
        res += qhull(far_vertex, p2, p2b, -1)

    return res

def quickhull(A):
    res = []

    S = sorted(A, key=lambda x: x[0])
    p1 = S[0]
    p2 = S[-1]
    
    res += [p1, p2]

    S.pop(0)
    S.pop(-1)

    res += qhull(p1, p2, create_segment(p1, p2, S)[0], 1)
    res += qhull(p1, p2, create_segment(p1, p2, S)[1], -1)

    res.append(res[0])
    return res

def random_list(n):
    R = []
    for _ in range(n):
        R.append((random.randrange(100), random.randrange(100)))
    return R

def draw(T):
    fig = plt.figure()
    for v in A:
        plt.scatter(v[0], v[1], c='b')
    for u in T:
        plt.scatter(u[0], u[1], c = 'r')
        plt.plot(u[0],u[1])
    plt.show()

def rotate(A,B,C):
    return (B[0]-A[0])*(C[1]-B[1])-(B[1]-A[1])*(C[0]-B[0])

def Jarvis(A):
    n = len(A)
    P = list(range(n))
    # start point
    for i in range(1,n):
        if A[P[i]][0]<A[P[0]][0]: 
            P[i], P[0] = P[0], P[i]  
    H = [P[0]]
    del P[0]
    P.append(H[0])
    while True:
        right = 0
        for i in range(1,len(P)):
            if rotate(A[H[-1]],A[P[right]],A[P[i]])<0:
                right = i
        if P[right]==H[0]: 
                break
        else:
            H.append(P[right])
            del P[right]
    H.append(H[0])
    return [A[i] for i in H]

n = 1000

RAND = random_list(n)
A = []
[A.append(i) for i in RAND if i not in A]


V = Jarvis(A)

#V = quickhull(A)

print(V)

draw(V)
