import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import matplotlib.pyplot as plt 
import matplotlib as figure
import math

def rotate(a, b, c):
    u = [b[0]-a[0],b[1]-a[1]]
    v = [c[0]-b[0],c[1]-b[1]]
    return (u[0]*v[1]-u[1]*v[0])

def angle(a):
    angle = math.atan2(a[1],a[0])
    return(angle)

def GrehSort(a):
    return(a[2])

# = 1 if you want vizualization
viz = 1

f = open("input.txt")
points = []
convex = []
spoints = []
main_point = [2**63,2**63]
for i in f:
    a , b = map(int,i.split())
    if (main_point[0] > b) or (main_point[0] == b and main_point[1] >= a):
        if main_point != [2**63,2**63]:
            points.append(main_point)
            main_point = [a,b]
        else:
            main_point = [a,b]
    else:
        points.append([a,b])
for i in points:
    plt.scatter(i[0],i[1],color = "b")
plt.plot()
plt.scatter(main_point[0],main_point[1],color = "r")
plt.show()

for i in range(0,len(points)):
    points[i].append(angle([points[i][0]-main_point[0],points[i][1]-main_point[1]]))

points=sorted(points,key = GrehSort)

for i in range(0,len(points)-1):
    plt.scatter(points[i][0],points[i][1],color = "b",)
    plt.text(points[i][0],points[i][1],str(i),color = "b",)
plt.plot()
plt.scatter(main_point[0],main_point[1],color = "r")
plt.show()

points.append(main_point)
points.append(points[0])

s = []

s.append(main_point)
s.append(points[0])

for i in range(1,len(points)-1):
    while (len(s)>= 2) and ((rotate(s[len(s)-2],s[len(s)-1],points[i]))<0):
        s.pop()
    s.append(points[i])
    if viz:
        x = []
        y = []
        for j in s:
            x.append(j[0])
            y.append(j[1])
    
        plt.title("step {0}".format(i))
        for j in points:
            plt.scatter(j[0],j[1],color = "b")
        plt.plot()
        plt.scatter(main_point[0],main_point[1],color = "r")
        plt.plot(x,y)
        plt.show()

x = []
y = []
for i in s:
    x.append(i[0])
    y.append(i[1])

plt.title('Final')
for i in points:
    plt.scatter(i[0],i[1],color = "b")
plt.plot()
plt.scatter(main_point[0],main_point[1],color = "r")
plt.plot(x,y)
plt.show()

print("points =",points,main_point)
print("conv = ",s)
